var express     = require('express');
var request     = require('request');
var bodyParser  = require('body-parser');
var console     = require('./utils/console');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var node;

// 10-bits = 1024
var key_space_bits = 10;
var key_space = Math.pow(2, key_space_bits);

// Helper functions
// ----------------
// Calculate our key from ip:port
var computeKey = function(device_id)
{
    var crypto = require('crypto');
    // Get the hasher
    var sha256 = crypto.createHash('sha256');
    // Hash our ip+port and get the hex string out
    var hash = sha256.update("" + device_id).digest("hex").substring(0, 8);
    //console.info("HASH: " + hash);
    // Get our key (integer)
    var key = parseInt(hash, 16) % key_space;
    console.info("KEY: " + key);
    return key;
}

var join = function(ip, port)
{
    // Find our successor
    var url_find = 'http://' + ip + ':' + port + '/find_successor?id=' + node.key;
    request(url_find, {json: true}, function(error, response, body)
    {
        if(error)
        {
            console.error(error);
            // Retry in 10 seconds
            setTimeout(function(){join(ip,port)}, 10000);
            return;
        }

        console.log("My successor:" + JSON.stringify(body));
        var found_succ = body.object;

        var url_join = 'http://' + found_succ.ip + ':' + found_succ.port + '/resources';
        request.post(url_join,{json: {key: node}}, 
            function (error, response, body)
            {
                if(error)
                {
                    console.error(error);
                    // Retry in 10 seconds
                    setTimeout(function(){join(ip,port)}, 10000);
                    return;
                }

                console.log("Succesfully allocated resource!");
            });
    });
}

var device_id = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

var dateFormat = require('dateformat');

app.get('/data', function(req, res)
{
    var now = new Date();
    var time_string = dateFormat(now, "isoDateTime");
    // Remove timezone information
    time_string = time_string.substr(0, time_string.length-5)
    // Add milliseconds
    time_string = time_string + ".000Z";

    var data = {
        "cmd": "VarReturn",
        "name": "temperature",
        "result": Math.random() * 15 + 15,
        "coreInfo": {
            "last_app": "",
            "last_heard": time_string,
            "connected": true,
            "last_handshake_at": time_string,
            "deviceID": device_id
        }
    }

    res.json(data);
});

app.get('/ping', function(req, res)
{
    res.status(200);
    res.type("text/plain");
    res.send('pong');
});

app.post('/reassign', function(req, res)
{
	console.log("Reassign requested!");
	var ip = req.body.key.ip;
	var port = req.body.key.port;
	join(ip, port);
	res.status(200);
	res.type("text/plain");
	res.send('Succes');
});

var listener = app.listen(0, "0.0.0.0", function () 
{
    var ip = listener.address().address;
    var port = listener.address().port;

    console.info("Chord Node Spark running on:");
    console.info("Address: " + ip);
    console.info("Port: " + port);

    node = {
        ip: ip,
        port: port,
        key: computeKey(device_id),
        name: "Spark Core Mock (" + device_id + ")"
    }

    join("0.0.0.0", 8080);
});

