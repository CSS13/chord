// Library to spawn nodes
var spawn = require('child_process').spawn;
// Pretty console output
var console = require('./src/utils/console');

// Check that we got the parameters we need
if(process.argv.length < 3)
{
    console.error("Missing argument!");
    console.info("Usage: node nodespawner.js [NUMBER_OF_NODES]");
    process.exit(1);
}
var num_nodes = process.argv[2];

// Check that the number of nodes to spawn is valid
if(num_nodes < 1)
{
    console.error("Number of nodes to spawn must be atleast 1! (was " + num_nodes + ")");
    process.exit(1);
}

function spawn_node(node_name, command, output)
{
    var node = spawn('npm',  ['run', command]);
    if(output)
    {
        node.stdout.setEncoding('utf8');
        node.stdout.on('data', function (data) 
        {
            var str = data.toString();
            var lines = str.split(/(\r?\n)/g);
            console.raw(node_name + ":" + lines.join(""));
        });
        node.stderr.setEncoding('utf8');
        node.stderr.on('data', function (data) 
        {
            var str = data.toString();
            var lines = str.split(/(\r?\n)/g);
            console.raw(node_name + ":" + lines.join(""));
        });
    }
    node.on('close', function (code) 
    {
        console.warn(node_name + ': exit: ' + code);
    });
}

// Spawn the main node
console.info("Spawning main node!");
var main_node = spawn_node("Main Node", "start-main", true);

// Spawn child nodes
// TODO: Check against duplicate IDS
console.info("Spawning child nodes!");

var express = require('express');
var app = express();
var num_child_nodes = num_nodes - 1;
var last_child = 1;

app.post('/notify', function(req, res)
{
    last_child = last_child + 1;

    if(last_child > num_child_nodes)
    {
        console.log("Done spawning nodes!");
    }
    else
    {
        spawn_node("Node " + last_child, "start-node", true);
    }

    res.status(200);
    res.type("text/plain");
    res.send('Success');
});

var listener = app.listen(8000, "0.0.0.0", function () 
{
    var ip = listener.address().address;
    var port = listener.address().port;

    console.info("Chord Node Spawner running on:");
    console.info("Address: " + ip);
    console.info("Port: " + port);

    spawn_node("Node " + 0, "start-node", true);
});

