var express     = require('express');
var request     = require('request');
var bodyParser  = require('body-parser');
var console     = require('./utils/console');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var node;

// 10-bits = 1024
var key_space_bits = 10;
var key_space = Math.pow(2, key_space_bits);

// Helper functions
// ----------------
// Calculate our key from ip:port
var computeKey = function(device_id)
{
    var crypto = require('crypto');
    // Get the hasher
    var sha256 = crypto.createHash('sha256');
    // Hash our ip+port and get the hex string out
    var hash = sha256.update("" + device_id).digest("hex").substring(0, 8);
    //console.info("HASH: " + hash);
    // Get our key (integer)
    var key = parseInt(hash, 16) % key_space;
    console.info("KEY: " + key);
    return key;
}

var join = function(ip, port)
{
    // Find our successor
    var url_find = 'http://' + ip + ':' + port + '/find_successor?id=' + node.key;
    request(url_find, {json: true}, function(error, response, body)
    {
        if(error)
        {
            console.error(error);
            // Retry in 10 seconds
            setTimeout(function(){join(ip,port)}, 10000);
            return;
        }

        console.log("My successor:" + JSON.stringify(body));
        var found_succ = body.object;

        var url_join = 'http://' + found_succ.ip + ':' + found_succ.port + '/resources';
        request.post(url_join,{json: {key: node}}, 
            function (error, response, body)
            {
                if(error)
                {
                    console.error(error);
                    // Retry in 10 seconds
                    setTimeout(function(){join(ip,port)}, 10000);
                    return;
                }

                console.log("Succesfully allocated resource!");
            });
    });
}

var device_id    = '54ff72066667515108311467';
var access_token = 'fbb9ffb9d8f93ecacc7c157671086b879f30b7dc';

app.get('/data', function(req, res)
{
    var url_find = 'https://api.particle.io/v1/devices/' + device_id + '/temperature?access_token=' + access_token;
    request(url_find, {json: true}, function(error, response, body)
    {
        if(error)
        {
            console.error(error);
            process.exit(1);
        }
        console.warn(JSON.stringify(body));
        res.json(body);
    });
});

app.get('/ping', function(req, res)
{
    res.status(200);
    res.type("text/plain");
    res.send('pong');
});

app.post('/reassign', function(req, res)
{
	console.log("Reassign requested!");
	var ip = req.body.key.ip;
	var port = req.body.key.port;
	join(ip, port);
	res.status(200);
	res.type("text/plain");
	res.send('Succes');
});

var listener = app.listen(0, "0.0.0.0", function () 
{
    var ip = listener.address().address;
    var port = listener.address().port;

    console.info("Chord Node Spark running on:");
    console.info("Address: " + ip);
    console.info("Port: " + port);

    node = {
        ip: ip,
        port: port,
        key: computeKey(device_id),
        name: "Spark Core"
    }

    join("0.0.0.0", 8080);
});

