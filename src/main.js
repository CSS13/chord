var express     = require('express');
var console     = require('./utils/console');
var bodyParser  = require('body-parser');
var request     = require('request');
var FsKeyValue  = require('./utils/fs-key-value');
var mkdirp      = require('mkdirp');
var app = express();

app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));
app.use(bodyParser.json({limit: '50mb'}));

// 10-bits = 1024
var key_space_bits = 10;
var key_space = Math.pow(2, key_space_bits);
var num_fingers = key_space_bits;
var num_successors = 15;
var num_backups = 2;

var slim_node = function(node)
{
    if(!node)
    {
        return null
    }
    var slim = {
        ip: node.ip,
        port: node.port,
        key: node.key
    }
    return slim;
}

var Chord = function(ip, port)
{
    // Helper functions
    // ----------------
    // Calculate our key from ip:port
    var computeKey = function(ip, port)
    {
        var crypto = require('crypto');
        // Get the hasher
        var sha256 = crypto.createHash('sha256');
        // Hash our ip+port and get the hex string out
        var hash = sha256.update("" + ip + port).digest("hex").substring(0, 8);
        //console.info("HASH: " + hash);
        // Get our key (integer)
        var key = parseInt(hash, 16) % key_space;
        console.info("KEY: " + key);
        return key;
    }

    // Information about us
    this.ip = ip;
    this.port = port;
    this.key = computeKey(ip, port);
    // Information about nodes around us
    // INVARIANT [0] = null if we are alone in ring
    this.successor_list = [null];
    this.predecessor = null;
    // Escape 'this' for use inside functions
    var ident = this;
    // Finger table, one entry per bit
    this.finger_ids = [];
    this.fingers = [];
    // Resources table, one entry per resource node
    this.resources = [];
	// Nodes we are backing up
    this.nodes_backed_up = [];
    this.nodes_backing_up = [];
	this.resources_backed_up = {};

    var push_database_data = function(node)
    {
        var push_helper = function(id, readings, resource_key, database)
        {
            if(id == readings.length)
            {
                console.log("All data pushed!");
                // Start pushing new data
                ident.nodes_backing_up.push(node);
                return;
            }
            var reading = readings[id];
            console.log("Pushing reading: " + reading);

            ident.get_reading(database, reading, function(data)
            {
                var url = 'http://' + node.ip + ':' + node.port + '/reading';
                request.post(url, {json: {"key": resource_key, "name": reading, "data": data}},
                function(error, response, body)
                {
                    if(error)
                    {
                        // TODO: Rebuild backup table, or something   
                        console.error("Error pushing reading (backup-node dead?)");
                        console.error(error);
                        return;
                    }
                    push_helper(id+1, readings, resource_key, database);
                });
            }, function()
            {
                console.assert("NO DATA FOUND");
                console.assert("NODE: " + ident.key);
                console.assert("FIELD: " + reading_name);
                process.exit(1);
            });
        }

        for(var i = 0; i < ident.resources.length; i++)
        {
            (function(i)
            {
                var res = ident.resources[i];
                ident.get_database(res.key, function(database)
                {
                    database.list(function(error, data)
                    {
                        if(error)
                        {
                            console.assert("UNABLE TO LIST DATABASE CONTENT!");
                            console.assert("NODE: " + ident.key);
                            console.assert(error);
                            process.exit(1);
                        }

                        var readings = data.slice(1);
                        console.log(readings);
    
                        push_helper(0, readings, res.key, database);
                    });
                });
             })(i);
        }
    }
    
    this.delegate_backups = function()
    {
		for(var i = 0; i < num_backups; i++)
		{
			(function(i)
			{
				var delegate = ident.successor_list[i];
				if(delegate == null)
				{
					return;
				}
				var del_ip = delegate.ip;
				var del_port = delegate.port;
				var del_key = delegate.key;
				var url = 'http://' + del_ip + ':' + del_port + '/backup';
				request.post(url, {json: slim_node(ident)},
					function(error, response, body)
					{
						if(error)
						{
							console.error("Error delegating backuping");
							setTimeout(function(){ident.delegate_backups()}, 10000);
							return;
						}
                        // Push all our current data, to the backup
                        console.info("Pushing all current data to backup (" + delegate.key + ")");
                        push_database_data(delegate);
					});
			})(i);
		}
	}
    
    this.handle_backing_up = function()
    {
        for(var i = 0; i < ident.nodes_backed_up.length; i++)
	    {
			(function(i)
			{
				var node_ip = ident.nodes_backed_up[i].ip;
				var node_port = ident.nodes_backed_up[i].port;
                var node_key = ident.nodes_backed_up[i].key;
                query_backed_up_node(node_ip, node_port, node_key, i);
			})(i);
		}
    }

    var query_backed_up_node = function(node_ip, node_port, node_key, index)
    {
        var url = 'http://' + node_ip + ':' + node_port + '/ping';
        request(url, function(error, response, body)
		{
            if(error)
            {
                console.error("Node: key=" + node_key + " is not responding");
                console.log("Telling its resources to move on");

                // Instruct resources to move
                var list_of_resources = ident.resources_backed_up[node_key];
                for(var i = 0; i < list_of_resources.length; i++)
                {
                    (function(i)
                    {
                        var res = list_of_resources[i];
                        var res_ip = res.ip;
                        var res_port = res.port;
                        var url = 'http://' + res_ip + ':' + res_port + '/reassign';
                        request.post(url, {json: {key: ident}}, 
							function (error, response, body)
							{
								ident.nodes_backed_up.splice(index, 1);
                                if(error)
                                {
                                    console.assert("Error while reassigning!");
                                    console.assert(error);
                                }
							});
                    }(i));
                }
            }
        });
    }

	setInterval(function(){ ident.handle_backing_up(); }, 10000);
    
    this.handle_successor_churn = function()
    {
		// Bootstrapping support (we are alone in ring)
        if(ident.successor_list[0] == null)
        {
            return;
        }
        // Ping our successor
        var succ = ident.get_successor();
        var url = 'http://' + succ.ip + ':' + succ.port + '/ping';
        request(url, function(error, response, body)
        {
            if(error)
            {
				// Handle churn
				recursive_churn_helper(1);
            }
        });
    }
    
    var recursive_churn_helper = function(index)
	{
		var succ = ident.successor_list[index];
		if(succ == null)
		{
			// Its all over
			console.error("Last remaining known successor node has died.");
			console.log("Goodbye, cruel world.");
			process.exit(1);
            return;
        }
        
        var url = 'http://' + succ.ip + ':' + succ.port + '/ping';
        request(url, function(error, response, body)
        {
            if(error)
            {
				console.log("Successor: "+ succ.key +" isn't responding");
				recursive_churn_helper(index + 1);
            }
            else
            {
				// Success! a node in succ list is alive.
				// Handle churn
				
				ident.successor_list[0] = succ;
				node.create_successor_list(slim_node(succ));
				
                url_succ = 'http://' + succ.ip + ':' + succ.port + '/predecessor';
                request.post(url_succ, {json: {key: slim_node(ident)}}, 
                function(error,response, body)
                {
					if(error)
					{
						console.error("Failed to recover from lost successor.");
						console.log("Goodbye, cruel world.");
						process.exit(1);
					}
                });
			}
        });
	}

	// Refresh interval for successor lists
    setInterval(function(){ ident.handle_successor_churn(); }, 1000);

    this.check_consistancy = function(node, endpoint)
    {
        if(node == null)
        {
            return;
        }

        var url = 'http://' + node.ip + ':' + node.port + endpoint;
        request(url, {json: true}, function(error, response, body)
        {
            if(error)
            {
                console.error("Unable to connect successor!");
                return;
            }
            // Check that we're it!
            if(body.key != ident.key)
            {
                console.error("Ring inconsistant - " + endpoint + " (" + body.key + " != " + ident.key + ")");
                // Try to clean it up, claim that we're not the predecesssor of this node!
                request.post(url, {json: {key: ident}}, function(error, response, body)
                {
                    if(error)
                    {
                        console.error("Unable to consistanify ring!");
                        return;
                    }
                });
            }
        });
    }

	setInterval(function(){ ident.check_consistancy(ident.successor_list[0], '/predecessor'); }, 10000);
	setInterval(function(){ ident.check_consistancy(ident.predecessor, '/successor'); }, 10000);

    this.update_successor_list = function()
	{
		if(ident.successor_list[0] != null)
		{
			ident.create_successor_list(ident.successor_list[0]);
		}
	}
	
	this.get_successor = function()
	{
		return ident.successor_list[0];
	}
	
	this.create_successor_list = function(succ)
	{
		ident.successor_list[0] = succ;
		successor_list_helper(succ, 0, []);
	}
	
	// Recursively construct successor list by asking each new succ for its succ
	// until we have enough.
	var successor_list_helper = function(new_node, index, new_succ_list)
	{
		if(new_node == null || new_node.key == ident.key)
		{
			// Bootstrapping support, or node was same as self?
			// Not enough nodes in the ring for k successors?
			// Stop adding new successors.
			if(index == 0)
			{
				new_succ_list[index] = null;
			}
			ident.successor_list = new_succ_list;
			return;
		}
		// Add the given node to the successor list
		new_succ_list[index] = new_node;
		
		// Get successor of given node and call this function recursively.
		var url = 'http://' + new_node.ip + ':' + new_node.port + '/successor';
		request(url, {json:true}, function(error, response, body)
			{
				if(error)
				{
					console.error("Node ID: " + ident.key + 
								" Failed creating successor list, node ID not responding: "
								+ new_node.key );
					console.error(error);
					return;
				}
				else
				{
					var found_node = body;
					if(index < (num_successors - 1))
					{
						successor_list_helper(found_node, (index + 1), new_succ_list);
					}
					else // Done, set the successor list atomically.
					{
						ident.successor_list = new_succ_list;
					}
				}
			});
	}
	
	// Refresh interval for successor lists
    setInterval(function(){ ident.update_successor_list(); }, 10000);
	
    // Pull in resource data
    this.pull_resource_data = function(res, callback)
    {
        var url = 'http://' + res.ip + ':' + res.port + '/data';
        request(url, {json: true}, function(error, response, body)
        {
            if(error)
            {
                // Warn that resource is gone
                console.error("Resource: key=" + res.key + " is not responding!");
                console.error(error);
                // Return
                return;
            }
            callback(body);
        });
    }

    this.get_database = function(resource_key, callback)
    {
        mkdirp('./database/' + ident.key, function(err)
        {
            new FsKeyValue('./database/' + ident.key + '/' + resource_key, 
                function (err, node_db) 
                {
                    if (err)
                    {
                        console.assert("UNABLE TO OPEN DATABASE");
                        console.assert("NODE: " + ident.key);
                        console.assert("RESOURCE: " + res.key);
                        console.assert(err);
                        process.exit(1);
                    }

                    callback(node_db);
                });
        });
    }

    this.save_reading_id = function(database, reading, callback)
    {
        // Acquire a spot
        database.put('reading', reading + 1, function(err)
        {
            if(err)
            {
                console.assert("UNABLE TO ACQUIRE NODE DATABASE ID");
                console.assert("NODE: " + ident.key);
                console.assert("FIELD: " + 'reading');
                console.assert(err);
                process.exit(1);
            }
            callback(reading);
        });
    }

    this.save_reading = function(database, reading_name, reading, callback)
    {
        // Output our data
        database.put("" + reading_name, reading, function (err)
        {
            if(err)
            {
                console.assert("UNABLE TO WRITE DATABASE");
                console.assert("NODE: " + ident.key);
                console.assert("FIELD: " + reading_name);
                console.assert(err);
                process.exit(1);
            }
            callback();
        });
    }

    this.get_reading = function(database, reading_name, callback, no_data_callback)
    {
        database.get(reading_name, function(err, data)
        {
            if(err)
            {
                console.assert("UNABLE TO READ DATABASE");
                console.assert("NODE: " + ident.key);
                console.assert("FIELD: " + reading_name);
                console.assert(err);
                process.exit(1);
            }
            if ( data == undefined )
            {
                no_data_callback();
                return;
            }
            callback(data);
        });
    }

    var data_puller = function()
    {
        var get_reading_id = function(database, callback)
        {
            database.get('reading', function(err, data)
            {
                if(err)
                {
                    console.assert("UNABLE TO READ DATABASE");
                    console.assert("NODE: " + ident.key);
                    console.assert("FIELD: " + 'reading');
                    console.assert(err);
                    process.exit(1);
                }
                var reading = 0;
                if ( data == undefined )
                {
                    console.warn("Bootstrapping node! " + ident.key);
                }
                else
                {
                    //console.log(data);
                    reading = parseInt(data);
                }
                ident.save_reading_id(database, reading, callback);
            });
        }


        for(var i = 0; i < ident.resources.length; i++)
        {
            (function(i)
             {
                 var res = ident.resources[i];
                 ident.pull_resource_data(res, function(data)
                 {
                     ident.get_database(res.key, function(database)
                     {
                         get_reading_id(database, function(name)
                         {
                             // Save the reading locally
                             ident.save_reading(database, name, data, function()
                             {
                                 // Send the reading to our backup nodes
                                 for(j = 0; j < num_backups; j++)
                                 {
                                     (function(i)
                                     {
                                          var backup = ident.nodes_backing_up[i];
                                          if(!backup)
                                          {
                                              return;
                                          }
                                          var url = 'http://' + backup.ip + ':' + backup.port + '/reading';
                                          request.post(url, {json: {"key": res.key, "name": name, "data": data}},
                                              function(error, response, body)
                                              {
                                                  if(error)
                                                  {
                                                      // TODO: Rebuild backup table, or something   
                                                      console.error("Error pushing reading (backup-node dead?)");
                                                      console.error(error);

                                                      ident.nodes_backing_up = [];
                                                      ident.delegate_backups();
                                                      return;
                                                  }
                                              });
                                      })(j);
                                  }
                             })
                         });
                     });
                 });
             })(i);
        }
    }

    // Pull resources for data
    setInterval(function(){ data_puller(); }, 10000);
    
    // Request that all resources find new responsible node
    this.update_resources = function(res_list)
    {
		for(var i = 0; i < res_list.length; i++)
		{
			(function(i)
			{
				var res_ip = res_list[i].ip;
				var res_port = res_list[i].port; 
				ident.send_reassign(res_ip, res_port, ident.predecessor);
			})(i);
		}
	}
	
	// Request that resources move to new node,
	// when resources ID puts them behind new predecessor.
	this.update_resources_pred = function()
	{
		for(var i = 0; i < ident.resources.length; i++)
		{
			(function(i)
			{
				var res_key = ident.resources[i].key;
				if(ident.check_fingers(ident.key, res_key, ident.predecessor.key))
				{
					var res_ip = ident.resources[i].ip;
					var res_port = ident.resources[i].port; 
					ident.send_reassign(res_ip, res_port, ident.predecessor);
					ident.resources.splice(i, 1);
				}
			})(i);
		}
	}
	
	// tell a resource to ask node for assignment
	this.send_reassign = function(ip, port, responsible_node)
	{
		var url = 'http://' + ip + ':' + port + '/reassign';
		request.post(url, {json: {key: responsible_node}}, 
				function(error, response, body)
				{
					if(error)
                     {
                         // Warn that resource is gone
                         console.error("Resource at: " + url + " DIED!");
                         console.error(error);
                         // Return
                         return;
                     }
				});
	}

    // Clear out dead resources
    this.flush_resources = function()
    {
        //console.info("Resource Check");
        for(var i = 0; i < ident.resources.length; i++)
        {
            (function(i)
             {
                 var url = 'http://' + ident.resources[i].ip + ':' + ident.resources[i].port + '/ping';
                 request(url, {json: true}, function(error, response, body)
                 {
                     if(error)
                     {
                         // Warn that resource is gone
                         console.error("Resource: id=" + i + " DIED!");
                         console.error("Querying: " + url);
                         console.error(error);
                         // Remove it
                         ident.resources.splice(i, 1);
                         // Return
                         return;
                     }
                     // Notify that resource lives
                     console.info("Resource: " + i + " survived ping");
                 });
             })(i);
        }
    }
    // Ping resources every minute
    setInterval(function(){ ident.flush_resources(); }, 10000);

    this.build_fingers = function()
    {
        for(var i = 0; i < num_fingers; i++)
        {
            var index_id = (ident.key + (Math.pow(2, i)) ) % (Math.pow(2,num_fingers));
            ident.finger_ids[i] = index_id;
        }
    }

    this.update_fingertable = function(callback)
    {
        // Build a new finger table in this variable
        var new_finger_table = [];
        new_finger_table[0] = ident.get_successor();
        for(var i = 1; i < num_fingers; i++)
        {
            (function(i)
            {
                ident.find_successor(ident.finger_ids[i], function(resp)
                {
                    var found = resp.object;
                    if(found.key == ident.key)
                    { 
                        // found is self, dont use.
                        new_finger_table[i] = null;
                    }
                    else
                    {
                        new_finger_table[i] = found;
                    }
                    // Once all fingers has been set in the new table, flip it
                    // I.e. replace the old finger table with the new one
                    var size = new_finger_table.filter(function(value) { return value !== undefined }).length;
                    if(size == num_fingers)
                    {
                        //console.info("Node(" + ident.key + ") finger table built, flipping!");
                        ident.fingers = new_finger_table;
                        callback();
                    }
                });
            })(i);
        }
    }
    // Update finger table every 10 seconds
    setInterval(function(){ ident.update_fingertable(function() {}); }, 10000);
    
    this.find_successor = function(id, callback, debug)
    {
        var callback_incrementer = function(object)
        {
            object.relay = object.relay + 1;
            callback(object);
        }

		//console.log("Node " + ident.key + " looking for successsor to: " + id);
        // Base case
        if(ident.get_successor() == null || id == ident.key)
        {
            if(debug)
                console.warn("Return call, base case, from key: " + ident.key);

            var response = {
                relay: 0,
                object: slim_node(ident)
            }
            callback(response);
        }
        // Between us and our imidate successor
        else if(ident.check_fingers(ident.key, id, ident.get_successor().key))
        {
            if(debug)
                console.warn("Return call, successor, from key: " + ident.key);

            var response = {
                relay: 0,
                object: slim_node(ident.get_successor())
            }
            callback(response);
        }
        // We need to pass the request on to 'someone'
        else
        {
			// Attempt to use finger table.
            for(var i = num_fingers; i > 1; i--)
            {
                var prev_finger = ident.fingers[i-1];
				var finger = ident.fingers[i];

                // We're missing finger table entries, roll through them
                if(prev_finger == null)
                    continue;

                // 10-case (between last finger in table and us)
                if(finger == null)
                    finger = ident;

                // Check if we need to jump to prev_finger
                if(!(ident.check_fingers(prev_finger.key, id, finger.key)))
                {
                    // Not this finger, keep looking!
                    continue;
                }

                // We need to jump to prev_finger!
                if(debug)
                    console.warn("Finger call: " + i + " from key: " + ident.key + " against key " + prev_finger.key);

                // Jump to prev finger
                ident.query_for_successor(prev_finger, id, callback_incrementer, function()
                {
                    console.assert("Unable to find_successor using finger-table, falling back to successor");
                    console.assert("Successor is: " + ident.get_successor().key);
                    ident.query_for_successor_list(0, id, callback_incrementer, function()
                    {
                        console.assert("Unable to gracefully handle broken finger-table");
                        process.exit(1);
                    }, debug);
                }, debug);

                // We set a jump in motion, just return and wait for callback
                return;
            }
            
            if(debug)
                console.warn("Successor call from key: " + ident.key);
        
            // Jump to successor (fallback)
			ident.query_for_successor_list(0, id, callback_incrementer, function()
            {
                console.assert("Unable to query for successor (fallback)");
                process.exit(1);
            }, debug);
        }
    };

    this.check_fingers = function(key1, id, key2)
    {
        // Insert between us and the successor (no wrap around)
        return (key1 < id && id <= key2
        // ident --> 0 --> successor
        // 0 < id < successor
             || key1 > key2 && id < key2
        // ident < id < max
             || key1 > key2 && key1 < id);
    }

    this.query_for_successor_list = function(succ_id, id, callback, err_callback, debug)
    {
        var succ = ident.successor_list[succ_id];

        // This successor is null, we're out of luck!
        if(succ == null)
        {
            console.assert("No more successors to try!");
            err_callback();
        }
        // We have a successor to try!
        ident.query_for_successor(succ, id, callback, function()
        {
            // We failed using that successor
            console.error("Successor " + succ_id + " failed!");
            ident.query_for_successor_list(succ_id+1, id, callback, err_callback, debug);
        }, debug);
    }
    
    this.query_for_successor = function(finger, id, callback, err_callback, debug)
    {
        var url = 'http://' + finger.ip + ':' + finger.port + '/find_successor?id=' + id;
        if(debug)
            url = url + "&debug=true";

        request(url, {json: true}, function(error, response, body)
        {
            if(error)
            {
				if(err_callback == null)
                {	
					console.error(error);
					process.exit(1);
				}
				else
				{
					err_callback();
                    return;
				}
            }
            callback(body);
        });
    }

    this.notify_spawner = function()
    {
        request.post('http://0.0.0.0:8000/notify', function(error, response, body)
        {
            if(error)
            {
                console.assert("Unable to notify spawner!");
                console.assert(error);
                return;
            }
            //console.warn("Notified the spawner!");
        });
    }
    
    // Connect to ip:port and join the chord ring
    this.join = function(ip, port)
    {
        // Find our successor
        var url_find = 'http://' + ip + ':' + port + '/find_successor?id=' + ident.key;
        request(url_find, {json: true}, function(error, response, body)
        {
            if(error)
            {
                console.error(error);
                process.exit(1);
            }

            console.log("My successor:" + JSON.stringify(body));
            var found_succ = body.object;

            // Request to join behind our successor
            var url_join = 'http://' + found_succ.ip + ':' + found_succ.port + '/join';
            request.post(url_join,{json: {key: ident}}, 
                function (error, response, body)
            {
                console.log("My predecessor" + JSON.stringify(body));

                var found_pred = body;
                // Set successor and predecessor
                ident.create_successor_list(found_succ);
                ident.predecessor = found_pred;
                // TODO: Inherit a finger table
                //ident.fingers = found_pred.fingers;
                ident.update_fingertable(function()
                {
                    // Notify the spawner that it can create another node
                    ident.notify_spawner();
                });
            });
        });
    }
}

// This chord node
var node;

var find_key_in_node_array = function(arr, key)
{
	for(i = 0; i < arr.length; i++)
	{
		var node = arr[i];
		var node_key = node.key;
		if(node_key == key)
		{
			return true;
		}
	}
	return false;
}

app.get('/', function (req, res)
{
    // Status code 200
    res.status(200);
    res.type('html');
    res.sendFile('res/index.html', {root: __dirname});
});

app.get('/ping', function (req, res)
{
	res.status(200);
	res.type("text/plain");
	res.send('pong');
});

app.post('/backup', function(req,res)
{
    // Node asking us to do backup of its resources.
    var res_owner = req.body;
    
	if(!find_key_in_node_array(node.nodes_backed_up, res_owner.key))
	{
		// Add new resource owner to list of nodes backed up
		node.nodes_backed_up.push(res_owner);
	}
    
    var owner_ip = res_owner.ip;
    var owner_port = res_owner.port;
    var url_resources = 'http://' + owner_ip + ':' + owner_port + '/resources';
    request(url_resources,{json: true}, function (error, response, body)
    {
        node.resources_backed_up[res_owner.key] = body;

		res.status(200);
		res.type("text/plain");
		res.send('Success');
    });
});

app.get('/this', function (req, res)
{
    res.json(node);
});

app.get('/find_successor', function (req, res)
{
    var requested_id = req.query.id;
    var requested_debug = req.query.debug;
    var resp = node.find_successor(requested_id, function(resp)
    {
        res.json(resp);
    }, requested_debug);
});

app.get('/successor', function (req, res)
{
    res.json(node.get_successor());
});

app.post('/successor', function (req, res)
{
    var new_successor = req.body.key;
    if(new_successor.key == node.key)
    { // node is self, dont set
		node.create_successor_list(null);
	}
	else
	{
		node.create_successor_list(slim_node(new_successor));
	}

	node.update_fingertable(function()
    {
        res.status(200);
        res.type("text/plain");
        res.send('Success');
    });
});

app.get('/predecessor', function (req, res)
{
    res.json(node.predecessor);
});

app.post('/predecessor', function (req, res)
{
    var new_predecessor = req.body.key;
    node.predecessor = slim_node(new_predecessor);
    node.update_resources_pred();

    node.update_fingertable(function()
    {
        res.status(200);
        res.type("text/plain");
        res.send('Success');
    });
});

app.get('/fingers', function (req,res)
{
    res.json(node.fingers);
});

app.post('/fingers', function(req, res)
{
    node.update_fingertable(function()
    {
        res.status(200);
        res.type("text/plain");
        res.send('Success');
    });
});

// TODO: Save the passed reading to our local folder
app.post('/reading', function(req, res)
{
    var resource_key = req.body.key;
    var reading_name = req.body.name;
    var reading = req.body.data;

    console.info("Saving " + reading_name + " at " + resource_key + " with payload: " + JSON.stringify(reading));

    node.get_database(resource_key, function(database)
    {
        node.save_reading(database, reading_name, reading, function()
        {
            node.save_reading_id(database, reading_name, function()
            {
                res.status(200);
                res.type("text/plain");
                res.send('Success');
            });
        });
    });
});

app.get('/reading/:resource_key', function(req, res)
{
    var res_key = req.params.resource_key;
    node.get_database(res_key, function(database)
    {
        database.list(function(error, data)
        {
            if(error)
            {
                res.status(404);
                res.type("text/plain");
                res.send("No such resource key");
                return;
            }

            var readings = data.slice(1,-1);
            console.log(readings);

            res.json(readings);
        });
    });
});

app.get('/reading/:resource_key/:reading_name', function(req, res)
{
    var res_key = req.params.resource_key;
    var reading = req.params.reading_name;
    node.get_database(res_key, function(database)
    {
        node.get_reading(database, reading, function(data)
        {
            res.json(data);
        }, function()
        {
            res.status(404);
            res.type("text/plain");
            res.send("No such reading exists / No such resource key");
        });
    });
});

// TODO: Pass on resources to our successor whenever required
app.get('/resources', function(req, res)
{
    // Return a list of nodes
    res.json(node.resources);
});

app.get('/resources/:id', function(req, res)
{
    var id = req.params.id;
    // Verify id validity
    if(id < node.resources.length)
    {
        // Pull data from node
        var resource = node.resources[id];
        node.pull_resource_data(resource, function(json)
        {
            res.json(json);
        });
    }
    else
    {
        // Report invalid ID
        res.status(404);
        res.type("text/plain");
        res.send('No such ID');
    }
});

app.post('/resources', function(req, res)
{
    var key = req.body.key;
    var new_resource = key;
	if(!find_key_in_node_array(node.resources, new_resource.key))
	{
		console.log("Added resource(" + node.key + "): " + JSON.stringify(new_resource));

		node.resources.push(new_resource);
		node.delegate_backups();
	}
    res.status(200);
    res.type("text/plain");
    res.send('Success');
});

app.post('/leave', function (req, res)
{
    var succ = node.get_successor();
    var pred = node.predecessor;
    url_pred = 'http://' + node.predecessor.ip + ':' + node.predecessor.port + '/successor';
    url_succ = 'http://' + node.get_successor().ip + ':' + node.get_successor().port + '/predecessor';
    
    // Connect our successor and predecessor
    request.post(url_pred, {json: {key: succ}}, 
        function(error,response, body)
    {
        request.post(url_succ, {json: {key: pred}}, 
            function(error,response, body)
        {
            res.json(node.get_successor());
            node.update_resources(node.resources);
            // Wait for posts to get sent, and exit
            
            setTimeout( function(){process.exit(0);}, 5000);
        });
    });
});

app.post('/crash', function (req, res)
{
    process.exit(1);
});

// TODO: Return error codes, and document them in API

app.post('/join', function (req, res)
{
    var joinee = req.body.key;

    // TODO: Check id of joinee

    //console.warn("JOIN" + JSON.stringify(joinee));
    console.info("Node: " + joinee.key + " joining at node: " + node.key);
    
    // Inform predecessor of change
    var url_pred;
    if(node.predecessor == null)
    {
        url_pred = 'http://' + node.ip + ':' + node.port + '/successor';
    }
    else
    {
        url_pred = 'http://' + node.predecessor.ip + ':' + node.predecessor.port + '/successor';
    }
    request.post(url_pred, {json: {key: joinee}}, 
      function(error,response, body)
      {
          // TODO: Check that we succeeded in setting the successor of our predecessor
      });

    node.update_fingertable(function()
    {
        // Respond to join requestee
        if(node.predecessor == null)
        {
            res.json(node);
        }
        else
        {
            res.json(node.predecessor);
        }

        // Change own pred
        node.predecessor = slim_node(joinee);
        node.update_resources_pred();
    });
});

// The port to start the server on
// Defaults to 0 (random)
var init_port = 0;
if(process.argv.length >= 3)
{
    init_port = process.argv[2];
}
// Are we the main node (i.e. no join)
var node_type = "child";
if(process.argv.length >= 4)
{
    node_type = process.argv[3];
    //console.log("Main node!");
}
else
{
    //console.log("Child node!");
}

var listener = app.listen(init_port, "0.0.0.0", function () 
{
    var ip = listener.address().address;
    var port = listener.address().port;

    console.info("Chord Node running on:");
    console.info("Address: " + ip);
    console.info("Port: " + port);

    node = new Chord(ip, port);
    // Build fingers
    node.build_fingers();
    // If we're not the main node, try to join using the main node
    if(node_type == "child")
    {
        console.log("Joining chord ring");
        // TODO: This provided via command line
        node.join("0.0.0.0", "8080");
    }
});
