#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

mkdir -p html
docker run -it -v $PWD:/srv/ davidonlaptop/aglio:2.1.0 -i /srv/src/main.apib -o /srv/html/output.html

cd html; python -m SimpleHTTPServer
